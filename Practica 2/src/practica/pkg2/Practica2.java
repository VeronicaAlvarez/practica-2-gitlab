/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica.pkg2;

import java.util.Scanner;

/**
 *
 * @author DAM101
 */
public class Practica2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner sc = new Scanner(System.in);
        
        double datos;
        int opcion=0;
        
        //creamos el menú que va a observar el cliente para despues elegir lo que desea realizar
        System.out.println("-----Menus-----");
        System.out.println("1-Cambio de unidades de horas a segundo");
        System.out.println("2-Cambio de unidades de kilometros a metros");
        System.out.println ("3- Cambio de Km/h a M/s");
        System.out.println("3-Salir");
        
        //hasta que no se elija la opción 4, el programa va a seguir pidiendote un opción
        while (opcion!=4){
            System.out.println("Que opción desea elegir");
            opcion=sc.nextInt();
            
            //lo que deberá realizar dependiendo de que opción elija
            switch (opcion){
                case 1:
                    System.out.println("Se ha elegido cambio de unidades de horas a segundos");
                    System.out.println("Introduce las horas");
                    datos=sc.nextDouble();
                    datos=datos*3600;
                    System.out.println("Las horas introducidas son " +datos+ " segundos");
                    break;
                case 2:
                    System.out.println("Se ha elegido cambio de unidades de kilometros a metros");
                    System.out.println("Introduce los kilometros");
                    datos=sc.nextDouble();
                    datos=datos*1000;
                    System.out.println("Los kilometros introducidos son " +datos+ " metros");
                    break;
                case 3: 
                    System.out.println("Se ha elegido cambio de km/h a m/s");
                    System.out.println("Introduce los k/m");
                    datos=sc.nextDouble();
                    datos=datos*3.6;
                    System.out.println("Los km/h introducidos son " +datos+ " m/s");

                case 4:
                     System.out.println("Se ha elegido salir del programa");
                     System.exit(0);
            }
        }
        
    }//public void
    
}//public class
